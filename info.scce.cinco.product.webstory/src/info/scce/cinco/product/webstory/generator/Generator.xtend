package info.scce.cinco.product.webstory.generator;

import de.jabc.cinco.meta.core.utils.EclipseFileUtils
import de.jabc.cinco.meta.core.utils.projects.ProjectCreator
import de.jabc.cinco.meta.plugin.generator.runtime.IGenerator
import info.scce.cinco.product.webstory.webstory.WebStory
import org.eclipse.core.resources.IFolder
import org.eclipse.core.resources.IProject
import org.eclipse.core.runtime.IPath
import org.eclipse.core.runtime.IProgressMonitor

import info.scce.cinco.product.webstory.mcam.cli.WebStoryExecution

class Generator implements IGenerator<WebStory> {
	
	var IFolder jsStaticFolder
	var IFolder jsGenFolder
	var IFolder mainFolder

	override void generate(WebStory story, IPath targetDir, IProgressMonitor monitor) {
		
		if (story.hasErrors)
			throw new RuntimeException("Model has errors. Please fix before generating")	

		
		val IProject project = ProjectCreator.getProject(story.eResource())
		mainFolder = project.getFolder("generated-html")

		jsStaticFolder = mainFolder.getFolder("js_static")
		EclipseFileUtils.mkdirs(jsStaticFolder)

		jsGenFolder = mainFolder.getFolder("js_generated")
		EclipseFileUtils.mkdirs(jsGenFolder)
				
		val CharSequence generatedDeclarations = new VariableDeclarationsGenerator().generate(story)
		EclipseFileUtils.writeToFile(jsGenFolder.getFile("declarations.js"), generatedDeclarations)
		
		val CharSequence generatedActivities = new ActivityGenerator().generate(story)
		EclipseFileUtils.writeToFile(jsGenFolder.getFile("activities.js"), generatedActivities)
		
		copyStaticResources()
			
	}
	
	def copyStaticResources() {
		val thisBundle = "info.scce.cinco.product.webstory"
		EclipseFileUtils.copyFromBundleToDirectory(thisBundle, "resources/js/jquery.min.js", jsStaticFolder)	
		EclipseFileUtils.copyFromBundleToDirectory(thisBundle, "resources/js/webstoryFramework.js", jsStaticFolder)	
		EclipseFileUtils.copyFromBundleToDirectory(thisBundle, "resources/index.html", mainFolder)	
	}
	
	/**
	 *  Performs all available checks on the given web story
	 */
	static def hasErrors(WebStory story) {
		val wse = new WebStoryExecution()
		val checkAdapter = wse.initApiAdapter(EclipseFileUtils.getFileForModel(story))
		wse.executeCheckPhase(checkAdapter).hasErrors
	}

}
